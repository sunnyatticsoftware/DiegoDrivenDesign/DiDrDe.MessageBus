﻿using System;

namespace DiDrDe.MessageBus.Infra.MassTransit.Exceptions
{
    public class CouldNotStartMessageBusException
        : Exception
    {
        public CouldNotStartMessageBusException(string message)
            : base(message)
        {
        }

        public CouldNotStartMessageBusException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}