﻿using DiDrDe.MessageBus.Messages;
using MassTransit;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus.Infra.MassTransit
{
    public class EventConsumerAdapter<TEvent>
        : IConsumer<TEvent>
            where TEvent : class, IEvent
    {
        private readonly IEventConsumer<TEvent> _eventConsumer;

        public EventConsumerAdapter(IEventConsumer<TEvent> eventConsumer)
        {
            _eventConsumer = eventConsumer;
        }

        public async Task Consume(ConsumeContext<TEvent> context)
        {
            var message = context.Message;
            await _eventConsumer.Consume(message);
        }
    }
}