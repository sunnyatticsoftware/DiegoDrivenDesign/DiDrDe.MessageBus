﻿using DiDrDe.MessageBus.Messages;
using MassTransit;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus.Infra.MassTransit
{
    public class EventBus
        : IEventBus
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public EventBus(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task Publish<TEvent>(TEvent eventDto)
            where TEvent : class, IEvent
        {
            await _publishEndpoint.Publish(eventDto);
        }
    }
}