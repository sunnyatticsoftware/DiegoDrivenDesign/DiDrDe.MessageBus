﻿using DiDrDe.Health;

namespace DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers
{
    public interface IReadinessMessageBusManager
        : IMessageBusManager, IReadiness
    {
    }
}