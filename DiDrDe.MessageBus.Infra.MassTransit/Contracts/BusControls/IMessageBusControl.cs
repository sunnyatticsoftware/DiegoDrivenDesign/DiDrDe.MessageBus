﻿using MassTransit;

namespace DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls
{
    public interface IMessageBusControl
        : IBusControl
    {
    }
}