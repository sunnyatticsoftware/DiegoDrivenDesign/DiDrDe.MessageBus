﻿using DiDrDe.MessageBus.Messages;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus.Infra.MassTransit
{
    public sealed class CommandConsumerAdapter<TCommand, TCommandResponse>
        : IConsumer<TCommand>
            where TCommand : class, ICommand
            where TCommandResponse : class, IMessage
    {
        private readonly ICommandConsumer<TCommand, TCommandResponse> _commandConsumer;

        public CommandConsumerAdapter(ICommandConsumer<TCommand, TCommandResponse> commandConsumer)
        {
            _commandConsumer = commandConsumer ?? throw new ArgumentNullException(nameof(commandConsumer));
        }

        public async Task Consume(ConsumeContext<TCommand> context)
        {
            var response = await _commandConsumer.Consume(context.Message);

            await context.RespondAsync(response);
        }
    }
}