﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using DiDrDe.MessageBus.Infra.MassTransit.Exceptions;
using DiDrDe.MessageBus.Infra.MassTransit.Model;
using MassTransit.Util;

namespace DiDrDe.MessageBus.Infra.MassTransit.BusManagers
{
    public class MessageBusManager<TBusControl>
        : ICommandConsumerMessageBusManager,
            ICommandSenderMessageBusManager,
            IEventConsumerMessageBusManager,
            IReadinessMessageBusManager,
            IDisposable
            where TBusControl : class, IMessageBusControl
    {
        private readonly TBusControl _busControl;

        public MessageBusManager(TBusControl busControl)
        {
            _busControl = busControl;
        }

        public void Dispose()
        {
            TaskUtil.Await(Stop);
        }

        public async Task Start(int maxMillisecondsWait)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(maxMillisecondsWait);
            var cancellationToken = cancellationTokenSource.Token;
            try
            {
                await _busControl.StartAsync(cancellationToken);
            }
            catch (Exception exception)
            {
                var seconds = maxMillisecondsWait / 1000;
                throw new CouldNotStartMessageBusException($"Could not start server after {seconds} seconds using {_busControl}", exception);
            }
        }

        public async Task Stop()
        {
            await _busControl.StopAsync();
        }

        public async Task<bool> IsReady()
        {
            var readyMessage = new ReadinessMessage("Are you ready?");
            try
            {
                const int cancelAfterMilliseconds = 10000;
                await Start(cancelAfterMilliseconds);
                await _busControl.Publish(readyMessage);
                return true;
            }
            catch (Exception exception)
            {
                await Console.Out.WriteAsync($"Message Bus not ready, it threw the following exception: {exception}");
                return false;
            }
        }
    }
}