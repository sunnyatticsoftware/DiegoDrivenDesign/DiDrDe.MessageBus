﻿using DiDrDe.MessageBus.Messages;
using System;
using System.Collections.Generic;

namespace DiDrDe.MessageBus.Infra.MassTransit.Configuration
{
    public sealed class CommandTypesOptions
    {
        private readonly Dictionary<Type, Type> _commandsTypes;
        public IReadOnlyDictionary<Type, Type> CommandTypes => _commandsTypes;

        public CommandTypesOptions()
        {
            _commandsTypes = new Dictionary<Type, Type>();
        }

        public CommandTypesOptions ConsumesCommand<TCommandType, TResponseType>()
            where TCommandType : ICommand
            where TResponseType : IMessage
        {
            return AddCommand<TCommandType, TResponseType>();
        }

        public CommandTypesOptions SendsCommand<TCommandType, TResponseType>()
            where TCommandType : ICommand
            where TResponseType : IMessage
        {
            return AddCommand<TCommandType, TResponseType>();
        }

        private CommandTypesOptions AddCommand<TCommandType, TResponseType>()
            where TCommandType : ICommand
            where TResponseType : IMessage
        {
            var commandType = typeof(TCommandType);
            var responseType = typeof(TResponseType);

            _commandsTypes.Add(commandType, responseType);

            return this;
        }
    }
}