﻿using DiDrDe.MessageBus.Messages;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus
{
    public interface ICommandConsumer<in TCommand, TCommandResponse>
        where TCommand : class, ICommand
        where TCommandResponse : class
    {
        Task<TCommandResponse> Consume(TCommand command);
    }

    public interface ICommandConsumer<in TCommand>
        where TCommand : class, ICommand
    {
        Task Consume(TCommand command);
    }
}