﻿using DiDrDe.MessageBus.Sample.Common.Messages;
using System;

namespace DiDrDe.MessageBus.Sample.CommandConsumer
{
    public class CommandOneResult
        : ICommandOneResult
    {
        public DateTime CreatedOn { get; }
        public CommandOneResult()
        {
            CreatedOn = DateTime.UtcNow;
        }
    }
}