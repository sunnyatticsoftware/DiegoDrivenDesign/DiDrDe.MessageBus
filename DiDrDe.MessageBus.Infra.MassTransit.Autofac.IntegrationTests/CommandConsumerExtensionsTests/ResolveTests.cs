﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac.IntegrationTests.TestSupport.Contracts;
using DiDrDe.MessageBus.Infra.MassTransit.BusManagers;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using FluentAssertions;
using MassTransit;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.Autofac.IntegrationTests.CommandConsumerExtensionsTests
{
    public static class ResolveTests
    {
        public class Given_A_Container_And_A_Registration_Of_ActiveMq_Command_Consumer_When_Resolving_An_ICommandConsumerMessageBusControl
            : Given_When_Then_Test
        {
            private IComponentContext _componentContext;
            private ICommandConsumerMessageBusControl _sut;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder
                    .Register(ctx =>
                    {
                        var consumer = Mock.Of<ICommandConsumer<IFakeCommand, IFakeResponse>>();
                        return consumer;
                    })
                    .As<ICommandConsumer<IFakeCommand, IFakeResponse>>();

                builder.RegisterActiveMqCommandConsumer(
                    options =>
                    {
                        options.ConsumesCommand<IFakeCommand, IFakeResponse>();
                    },
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = "foo",
                                Port = 12345,
                                EndpointName = "foo",
                                Username = "foo",
                                Password = "foo",
                                UseSsl = false
                            };
                        return messageBusOptions;
                    });

                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<ICommandConsumerMessageBusControl>();
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_BusControlWrapper()
            {
                _sut.Should().BeAssignableTo<BusControlWrapper>();
            }

            [Fact]
            public void Then_It_Should_Be_An_IBus()
            {
                _sut.Should().BeAssignableTo<IBus>();
            }
        }

        public class Given_A_Container_And_A_Registration_Of_ActiveMq_Command_Consumer_When_Resolving_A_ICommandConsumerMessageBusManager
            : Given_When_Then_Test
        {
            private IComponentContext _componentContext;
            private ICommandConsumerMessageBusManager _sut;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder
                    .Register(ctx =>
                    {
                        var consumer = Mock.Of<ICommandConsumer<IFakeCommand, IFakeResponse>>();
                        return consumer;
                    })
                    .As<ICommandConsumer<IFakeCommand, IFakeResponse>>();

                builder.RegisterActiveMqCommandConsumer(
                    options =>
                    {
                        options.ConsumesCommand<IFakeCommand, IFakeResponse>();
                    },
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = "foo",
                                Port = 12345,
                                EndpointName = "foo",
                                Username = "foo",
                                Password = "foo",
                                UseSsl = false
                            };
                        return messageBusOptions;
                    });

                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<ICommandConsumerMessageBusManager>();
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_MessageBusManager_Of_Its_BusControl()
            {
                _sut.Should().BeAssignableTo<MessageBusManager<ICommandConsumerMessageBusControl>>();
            }
        }
    }
}