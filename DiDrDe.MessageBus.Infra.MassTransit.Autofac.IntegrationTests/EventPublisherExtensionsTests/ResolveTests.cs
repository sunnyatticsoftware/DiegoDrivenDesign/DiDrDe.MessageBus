﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using FluentAssertions;
using MassTransit;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.Autofac.IntegrationTests.EventPublisherExtensionsTests
{
    public static class ResolveTests
    {
        public class Given_A_Container_And_A_Registration_Of_ActiveMq_Event_Publisher_When_Resolving_An_IPublishEndpoint
            : Given_When_Then_Test
        {
            private IComponentContext _componentContext;
            private IPublishEndpoint _sut;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder.RegisterActiveMqEventPublisher(
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = "foo",
                                Port = 12345,
                                EndpointName = "foo",
                                Username = "foo",
                                Password = "foo",
                                UseSsl = false
                            };
                        return messageBusOptions;
                    });

                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<IPublishEndpoint>();
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_BusControlWrapper()
            {
                _sut.Should().BeAssignableTo<BusControlWrapper>();
            }
        }

        public class Given_A_Container_And_A_Registration_Of_ActiveMq_Event_Publisher_When_Resolving_An_IEventBus
            : Given_When_Then_Test
        {
            private IComponentContext _componentContext;
            private IEventBus _sut;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder.RegisterActiveMqEventPublisher(
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = "foo",
                                Port = 12345,
                                EndpointName = "foo",
                                Username = "foo",
                                Password = "foo",
                                UseSsl = false
                            };
                        return messageBusOptions;
                    });

                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<IEventBus>();
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_EventBus()
            {
                _sut.Should().BeAssignableTo<EventBus>();
            }
        }
    }
}