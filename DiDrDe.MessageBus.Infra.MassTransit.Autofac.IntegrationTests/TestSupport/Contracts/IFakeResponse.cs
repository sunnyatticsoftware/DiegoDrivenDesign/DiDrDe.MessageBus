﻿using DiDrDe.MessageBus.Messages;

namespace DiDrDe.MessageBus.Infra.MassTransit.Autofac.IntegrationTests.TestSupport.Contracts
{
    public interface IFakeResponse
        : IMessage
    {
    }
}