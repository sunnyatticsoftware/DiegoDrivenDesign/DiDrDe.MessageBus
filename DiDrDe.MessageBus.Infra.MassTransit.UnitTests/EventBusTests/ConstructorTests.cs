﻿using FluentAssertions;
using MassTransit;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.EventBusTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventBus _sut;
            private IPublishEndpoint _publishEndpoint;

            protected override void Given()
            {
                _publishEndpoint = Mock.Of<IPublishEndpoint>();
            }

            protected override void When()
            {
                _sut = new EventBus(_publishEndpoint);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IEventBus()
            {
                _sut.Should().BeAssignableTo<IEventBus>();
            }
        }
    }
}