﻿using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport.Contracts;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.Configuration.EventTypesOptionsTests
{
    public static class ConsumesEventTests
    {
        public class Given_An_EventDtoConsumerAdapter_When_Consuming
            : Given_When_Then_Test
        {
            private int _expectedLenght;
            private EventTypesOptions _sut;

            protected override void Given()
            {
                _expectedLenght = 1;
                _sut = new EventTypesOptions();
            }

            protected override void When()
            {
                _sut.ConsumesEvent<IFakeEvent>();
            }

            [Fact]
            public void Then_It_Should_Have_One_Item_In_The_Event_Types_Property()
            {
                _sut.EventTypes.Count.Should().Be(_expectedLenght);
            }
        }
    }
}