﻿using DiDrDe.MessageBus.Sample.Common.Messages;
using System;

namespace DiDrDe.MessageBus.Sample.CommandSender
{
    public class CommandOne
        : ICommandOne
    {
        public string Text { get; }
        public DateTime CreatedOn { get; }

        public CommandOne(string text)
        {
            Text = text;
            CreatedOn = DateTime.UtcNow;
        }

        public override string ToString()
        {
            return $"Command created on {CreatedOn} with text: {Text}";
        }
    }
}