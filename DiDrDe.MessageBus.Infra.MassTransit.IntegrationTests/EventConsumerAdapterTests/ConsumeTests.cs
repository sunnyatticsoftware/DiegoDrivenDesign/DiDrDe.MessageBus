﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts;
using MassTransit;
using MassTransit.Util;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.EventConsumerAdapterTests
{
    public static class ConsumeTests
    {
        public class Given_An_ActiveMq_EventBus_With_A_Message_Published_When_Consuming
            : Given_When_Then_Test
        {
            private IEventBus _sut;
            private IFakeEvent _eventToSend;
            private IEventConsumerMessageBusManager _eventConsumerMessageBusManager;
            private Mock<IEventConsumer<IFakeEvent>> _eventConsumerMock;
            private bool _isEventConsumed;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder
                    .RegisterActiveMqEventPublisher(
                        context =>
                        {
                            var messageBusOptions =
                                new ActiveMqOptions
                                {
                                    HostName = ActiveMqTestsConstants.HostName,
                                    Port = ActiveMqTestsConstants.Port,
                                    Username = ActiveMqTestsConstants.Username,
                                    Password = ActiveMqTestsConstants.Password,
                                    UseSsl = ActiveMqTestsConstants.UseSsl,
                                    AutoDelete = ActiveMqTestsConstants.AutoDelete
                                };
                            return messageBusOptions;
                        });

                const string endpointName = "fakeEventEndpoint";
                var newId = Guid.NewGuid();
                var uniqueEndpointName = $"{endpointName}_{newId}";

                _isEventConsumed = false;

                builder
                    .Register(
                        ctx =>
                        {
                            _eventConsumerMock = new Mock<IEventConsumer<IFakeEvent>>();
                            _eventConsumerMock
                                .Setup(x => x.Consume(It.IsAny<IFakeEvent>()))
                                .Returns(Task.CompletedTask)
                                .Callback(() =>
                                {
                                    _isEventConsumed = true;
                                });
                            var eventConsumer = _eventConsumerMock.Object;
                            return eventConsumer;
                        })
                    .As<IEventConsumer<IFakeEvent>>();

                builder
                    .RegisterActiveMqEventConsumer(
                        options =>
                        {
                            options.ConsumesEvent<IFakeEvent>();
                        },
                        context =>
                        {
                            var messageBusOptions =
                                new ActiveMqOptions
                                {
                                    HostName = ActiveMqTestsConstants.HostName,
                                    EndpointName = uniqueEndpointName,
                                    Port = ActiveMqTestsConstants.Port,
                                    Username = ActiveMqTestsConstants.Username,
                                    Password = ActiveMqTestsConstants.Password,
                                    UseSsl = ActiveMqTestsConstants.UseSsl,
                                    AutoDelete = ActiveMqTestsConstants.AutoDelete
                                };
                            return messageBusOptions;
                        });

                var container = builder.Build();
                var componentContext = container.Resolve<IComponentContext>();
                _eventConsumerMessageBusManager = componentContext.Resolve<IEventConsumerMessageBusManager>();
                TaskUtil.Await(() => _eventConsumerMessageBusManager.Start());
                var publishEndpoint = componentContext.Resolve<IPublishEndpoint>();
                _sut = new EventBus(publishEndpoint);
                var id = GuidGenerator.Create(1);
                _eventToSend = new FakeEvent(id);
                TaskUtil.Await(() => _sut.Publish(_eventToSend));
            }

            protected override void When()
            {
                SpinWait.SpinUntil(() => _isEventConsumed, 10000);
            }

            [Fact]
            public void Then_It_Should_Consume_The_Event()
            {
                _eventConsumerMock.Verify(x => x.Consume(It.IsAny<IFakeEvent>()), Times.Once);
            }

            protected override void Cleanup()
            {
                base.Cleanup();
                TaskUtil.Await(() => _eventConsumerMessageBusManager.Stop());
            }
        }

        public class Given_An_ActiveMq_EventBus_With_A_Message_Published_When_Fail_Consuming
            : Given_When_Then_Test
        {
            private IEventBus _sut;
            private IAnotherFakeEvent _eventToSend;
            private IEventConsumerMessageBusManager _eventConsumerMessageBusManager;
            private Mock<IEventConsumer<IAnotherFakeEvent>> _eventConsumerMock;
            private int _retriesCount;
            private bool _hasUsedAllRetries;
            private int _expectedRetryAttempts;
            private int _maxTime;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder
                    .RegisterActiveMqEventPublisher(
                        context =>
                        {
                            var messageBusOptions =
                                new ActiveMqOptions
                                {
                                    HostName = ActiveMqTestsConstants.HostName,
                                    Port = ActiveMqTestsConstants.Port,
                                    Username = ActiveMqTestsConstants.Username,
                                    Password = ActiveMqTestsConstants.Password,
                                    UseSsl = ActiveMqTestsConstants.UseSsl,
                                    AutoDelete = ActiveMqTestsConstants.AutoDelete
                                };
                            return messageBusOptions;
                        });

                const string endpointName = "fakeEventEndpoint";
                var newId = Guid.NewGuid();
                var uniqueEndpointName = $"{endpointName}_{newId}";

                var retryPolicyOptions =
                    new RetryPolicyOptions
                    {
                        RetryAttempts = 3,
                        RetryIntervalMilliseconds = 500
                    };

                _retriesCount = 0;
                _hasUsedAllRetries = retryPolicyOptions.RetryAttempts <= _retriesCount;
                _maxTime = retryPolicyOptions.RetryAttempts * retryPolicyOptions.RetryIntervalMilliseconds;

                builder
                    .Register(
                        ctx =>
                        {
                            _eventConsumerMock = new Mock<IEventConsumer<IAnotherFakeEvent>>();
                            _eventConsumerMock
                                .Setup(x => x.Consume(It.IsAny<IAnotherFakeEvent>()))
                                .Callback(() =>
                                {
                                    _retriesCount++;
                                })
                                .Throws<Exception>();

                            var eventConsumer = _eventConsumerMock.Object;
                            return eventConsumer;
                        })
                    .As<IEventConsumer<IAnotherFakeEvent>>();

                builder
                    .RegisterActiveMqEventConsumer(
                        options =>
                        {
                            options.ConsumesEvent<IAnotherFakeEvent>();
                        },
                        context =>
                        {
                            var messageBusOptions =
                                new ActiveMqOptions
                                {
                                    HostName = ActiveMqTestsConstants.HostName,
                                    EndpointName = uniqueEndpointName,
                                    Port = ActiveMqTestsConstants.Port,
                                    Username = ActiveMqTestsConstants.Username,
                                    Password = ActiveMqTestsConstants.Password,
                                    UseSsl = ActiveMqTestsConstants.UseSsl,
                                    AutoDelete = ActiveMqTestsConstants.AutoDelete,
                                    RetryPolicyOptions = retryPolicyOptions
                                };
                            return messageBusOptions;
                        });

                var container = builder.Build();
                var componentContext = container.Resolve<IComponentContext>();
                _eventConsumerMessageBusManager = componentContext.Resolve<IEventConsumerMessageBusManager>();
                TaskUtil.Await(() => _eventConsumerMessageBusManager.Start());

                var publishEndpoint = componentContext.Resolve<IPublishEndpoint>();
                _sut = new EventBus(publishEndpoint);
                var id = GuidGenerator.Create(1);
                _eventToSend = new AnotherFakeEvent(id);
                TaskUtil.Await(() => _sut.Publish(_eventToSend));

                _expectedRetryAttempts = retryPolicyOptions.RetryAttempts;
            }

            protected override void When()
            {
                SpinWait.SpinUntil(() => _hasUsedAllRetries, _maxTime);
            }

            [Fact]
            public void Then_It_Should_Consume_The_Event_As_Many_Times_As_Retry_Attempts_Configured()
            {
                _eventConsumerMock.Verify(x => x.Consume(It.IsAny<IAnotherFakeEvent>()), Times.Exactly(_expectedRetryAttempts));
            }

            protected override void Cleanup()
            {
                base.Cleanup();
                TaskUtil.Await(() => _eventConsumerMessageBusManager.Stop());
            }
        }
    }
}