﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Consumers;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts;
using FluentAssertions;
using MassTransit.Util;
using System;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.CommandBusTests
{
    public static class SendTests
    {
        public class Given_An_ActiveMq_CommandBus_And_A_Command_When_Sending
            : Given_WhenAsync_Then_Test
        {
            private ICommandBus _sut;
            private IFakeCommand _commandToSend;
            private IFakeResponse _commandResult;
            private IFakeResponse _expectedCommandResult;
            private Exception _handledException;
            private ICommandConsumerMessageBusManager _commandConsumerMessageBusManager;
            private ICommandSenderMessageBusManager _commandSenderMessageBusManager;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder
                    .RegisterType<FakeCommandConsumer>()
                    .As<ICommandConsumer<IFakeCommand, IFakeResponse>>();

                const string endpointName = "commandFakeEndpoint";

                builder
                    .RegisterActiveMqCommandConsumer(
                        commandOptions =>
                        {
                            commandOptions.ConsumesCommand<IFakeCommand, IFakeResponse>();
                        },
                        context =>
                        {
                            var messageBusOptions =
                                new ActiveMqOptions()
                                {
                                    HostName = ActiveMqTestsConstants.HostName,
                                    EndpointName = endpointName,
                                    Port = ActiveMqTestsConstants.Port,
                                    Username = ActiveMqTestsConstants.Username,
                                    Password = ActiveMqTestsConstants.Password,
                                    UseSsl = ActiveMqTestsConstants.UseSsl,
                                    AutoDelete = ActiveMqTestsConstants.AutoDelete
                                };
                            return messageBusOptions;
                        });

                builder
                    .RegisterActiveMqCommandSender(
                        commandOptions =>
                        {
                            commandOptions.SendsCommand<IFakeCommand, IFakeResponse>();
                        },
                        context =>
                        {
                            var messageBusOptions =
                                new ActiveMqOptions
                                {
                                    HostName = ActiveMqTestsConstants.HostName,
                                    EndpointName = endpointName,
                                    Port = ActiveMqTestsConstants.Port,
                                    Username = ActiveMqTestsConstants.Username,
                                    Password = ActiveMqTestsConstants.Password,
                                    UseSsl = ActiveMqTestsConstants.UseSsl,
                                    AutoDelete = ActiveMqTestsConstants.AutoDelete
                                };
                            return messageBusOptions;
                        });

                var container = builder.Build();
                var componentContext = container.Resolve<IComponentContext>();

                _commandConsumerMessageBusManager = componentContext.Resolve<ICommandConsumerMessageBusManager>();
                _commandSenderMessageBusManager = componentContext.Resolve<ICommandSenderMessageBusManager>();
                _sut = componentContext.Resolve<ICommandBus>();
                var id = GuidGenerator.Create(1);
                _commandToSend = new FakeCommand(id);

                _commandConsumerMessageBusManager.Start();
                _commandSenderMessageBusManager.Start();

                _expectedCommandResult = new FakeResponse(id);
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    _commandResult = await _sut.Send<IFakeCommand, IFakeResponse>(_commandToSend);
                }
                catch (Exception exception)
                {
                    _handledException = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Raise_Any_Exception()
            {
                _handledException.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Return_The_Correct_Command_Result()
            {
                _commandResult.Should().BeEquivalentTo(_expectedCommandResult);
            }

            protected override void Cleanup()
            {
                base.Cleanup();
                TaskUtil.Await(() => _commandSenderMessageBusManager.Stop());
                TaskUtil.Await(() => _commandConsumerMessageBusManager.Stop());
            }
        }
    }
}