﻿using System;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport
{
    public class AnotherFakeEvent
        : IAnotherFakeEvent
    {
        public Guid Id { get; }

        public AnotherFakeEvent(Guid id)
        {
            Id = id;
        }
    }
}