﻿using System;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport
{
    public class FakeResponse
        : IFakeResponse
    {
        public Guid IdResponse { get; }

        public FakeResponse(Guid idResponse)
        {
            IdResponse = idResponse;
        }
    }
}