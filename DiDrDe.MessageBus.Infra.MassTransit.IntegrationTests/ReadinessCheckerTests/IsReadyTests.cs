﻿using Autofac;
using DiDrDe.Health;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport;
using FluentAssertions;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.ReadinessCheckerTests
{
    public static class IsReadyTests
    {
        public class Given_A_Registration_Of_ActiveMq_Readiness_Checker_With_Unreachable_Server_When_IsReady
            : Given_WhenAsync_Then_Test
        {
            private IReadiness _sut;
            private bool _result;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder.RegisterActiveMqReadinessChecker(
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = "unreachableUri",
                                Port = 12345,
                                EndpointName = "foo",
                                Username = "foo",
                                Password = "foo",
                                UseSsl = false,
                                AutoDelete = true
                            };
                        return messageBusOptions;
                    });

                var container = builder.Build();
                var componentContext = container.Resolve<IComponentContext>();
                _sut = componentContext.Resolve<IReadiness>();
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.IsReady();
            }

            [Fact]
            public void Then_It_Should_Not_Be_Ready()
            {
                _result.Should().BeFalse();
            }
        }

        public class Given_A_Registration_Of_ActiveMq_Readiness_Checker_With_Reachable_Server_When_IsReady
            : Given_WhenAsync_Then_Test
        {
            private IReadiness _sut;
            private bool _result;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder.RegisterActiveMqReadinessChecker(
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = ActiveMqTestsConstants.HostName,
                                EndpointName = "readiness",
                                Port = ActiveMqTestsConstants.Port,
                                Username = ActiveMqTestsConstants.Username,
                                Password = ActiveMqTestsConstants.Password,
                                UseSsl = ActiveMqTestsConstants.UseSsl,
                                AutoDelete = true

                            };
                        return messageBusOptions;
                    });

                var container = builder.Build();
                var componentContext = container.Resolve<IComponentContext>();
                _sut = componentContext.Resolve<IReadiness>();
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.IsReady();
            }

            [Fact]
            public void Then_It_Should_Be_Ready()
            {
                _result.Should().BeTrue();
            }
        }
    }
}